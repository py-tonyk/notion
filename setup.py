import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='tonyk-notion',
    version='0.1',
    packages=['tonyk_notion'],
    include_package_data=True,
    license='MIT',
    description="A py-tonyk implementation of Notion API",
    long_description=README,
    url='https://gitlab.com/py-tonyk/notion',
    author="Antoine Pinsard",
    author_email='antoine.pinsard+tonyk_notion@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
        'Topic :: Software :: Development :: Libraries :: Python Modules',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Utilities',
        'Operating System :: OS Independent',
    ],
)
