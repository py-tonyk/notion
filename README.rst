============
Tonyk Notion
============

A Py-Tonyk implementation of Notion API.

Quick Start
===========

Tonyk Notion is designed to be easy to try and build PoCs without any configuration.

It ships with an automatic mode that converts your database and property names into
Python identifiers.

For instance, let's say you have a database named "(dev) Tasks", with the following
properties:

* Author
* Created on
* Assignee
* Name
* Status
* Due Date
* Completed on

Here is how you would access it:

.. code-block:: python

    >>> from tonyk_notion import Notion
    >>> notion = Notion(token="secret_********")
    >>> notion.db.dev_tasks.properties
    {'author': <created_by WJ%5C%5E "Author">,
     'created_on': <created_time muWL "Created on">,
     'assignee': <people notion%3A%2F%2Ftasks%2Fassign_property "Assignee">,
     'name': <title title "Name">,
     'status': <status notion%3A%2F%2Ftasks%2Fstatus_property "Status">,
     'due_date': <date notion%3A%2F%2Ftasks%2Fdue_date_property "Due Date">}
     'completed_on': <date sHzD "Completed on">}
    >>> task = notion.db.dev_tasks["deadbeefc0ffeebadbabedec0dec0c0a"]
    >>> task.author
    <UserList: [Antoine Pinsard]>
    >>> task.created_on
    datetime.datetime(2024, 3, 18, 11, 32, tzinfo=datetime.timezone.utc)
    >>> task.assignee
    <UserList: [John Doe]>
    >>> task.name
    <RichText: Write the documentation of Tonyk Notion>
    >>> task.status
    <SelectOption In Progress>
    >>> task.due_date
    <Date 2024-04-30>
    >>> task.completed_on is None
    True

Data are automatically converted to relevant types and offer a seemless API so
you can use it as you expect.

For instance, on a ``people`` property, even if you only allow 1 person, Notion
will still return a list. With Tonyk Notion, UserList is a type that can be
accessed either as a list or a single User element:

.. code-block:: python

    >>> task.assignee
    <UserList: [John Doe]>
    >>> task.assignee.name
    'John Doe'
    >>> for assignee in task.assignee:
    ...     print(f" * {assignee.name}")
     * John Doe


Note that this is only a convenience so you can easily deal with types as you
expect. For instance, saving the hassle of writing ``task.assignee[0]`` while
you designed your database for a single assignee. Or ``task.due_date.start``
while you designed your database to handle "Due Date" as a non-range date.

This is however not 100% magic. Some methods might have different expected
resolution according to the context. I tried to map method resolutions to the
most meaningful behavior, but it might not always be what you expect. For
instance, ``"documentation" in task.name`` will check if "documentation" is
present in the plain text version of the rich text, but
``for x in task.name`` will *not* iterate over the plain text character by
character. It will iterate over all rich text elements.


Use a resilient database scheme
===============================

The automatic discovery of the database and property names is convenient to
get started and draw a PoC. However it's not safe if users in your teamspace
(or even you) can randomly change the name of the databases and properties.
This would very likely result in your application crashing as the name you
use in your code won't match anything anymore.

To avoid this, you can manually register your databases with the name you
want and map properties names you want to property IDs.

.. code-block:: python

    from tonyk_notion import Notion, DatabaseEntry

    notion = Notion(token="secret_********")

    @notion.db.register
    class Task(DatabaseEntry):
        id = "0ff1cebadb00t4b1dbadf00db16b00b5"
        _property_slugs_map = {
            "author": "WJ%5C%5E",
            "creation_time": "muWL",
            "assignee": "notion%3A%2F%2Ftasks%2Fassign_property",
            "name": "title",
            "status": "notion%3A%2F%2Ftasks%2Fstatus_property",
            "due_before": "notion%3A%2F%2Ftasks%2Fdue_date_property",
            "date_of_completion": "sHzD",
        }


.. code-block:: python

    >>> task = notion.db.Task["deadbeefc0ffeebadbabedec0dec0c0a"]
    >>> task.due_before
    <Date 2024-04-30>

Your app will still crash if properties are removed, but that I'm afraid I
can't do anything for you.

What I did however is save you the hassle of copy-pasting the IDs of the
properties to do your mapping.

.. code-block:: python

   >>> print(notion.db.dev_tasks.as_python_class())

Will show you the code to register your database as it's currently defined.


Versioning
==========

Tonyk Notion follows the **M.m.p** version scheme where **M** is Major,
**m** is minor, and **p** is patch.

* A Major version upgrade means backward incompatible changes were introduced.
* A minor version upgrade means new features were added or deprecated
  (deprecated features will raise warnings until they are removed in the next
  Major version).
* A patch version upgrade means bugs were fixed.

Note that as long as version 1.0.0 is not released, any minor version might
introduce backward incompatible changes without having raised deprecation
warnings on previous versions.

