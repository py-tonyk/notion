import requests
from functools import cached_property

from . import errors
from .databases import DatabaseLoader
from .pages import PageLoader
from .users import UserLoader

__all__ = ['Notion']


class Notion:

    @cached_property
    def tonyk_version(self):
        from . import __version__
        return __version__

    def __init__(self, *, token, version=None, api_url=None, user_agent=None):
        self.token = token
        self.version = version or "2022-06-28"
        self.api_url = api_url or 'https://api.notion.com/v1/'
        self.user_agent = user_agent or f"tonyk-notion {self.tonyk_version}"
        self.db = DatabaseLoader(self)
        self.pages = PageLoader(self)
        self.users = UserLoader(self)

    def _request(self, method, endpoint, **kwargs):
        url = self.api_url + endpoint
        try:
            r = requests.request(
                method,
                url,
                headers={
                    'Authorization': f'Bearer {self.token}',
                    'Notion-Version': self.version,
                    'Accept': 'application/json',
                    'User-Agent': self.user_agent,
                },
                **kwargs,
            )
        except ConnectionError as e:
            raise errors.NotionConnectionError(str(e)) from e
        if r.status_code == 204:
            return None
        elif 200 <= r.status_code < 300:
            return r.json()
        elif 400 <= r.status_code < 500:
            self._raise_client_error(r)
        elif 500 <= r.status_code < 600:
            self._raise_server_error(r)
        raise errors.UnexpectedResponse(r.status_code, r.text)

    def _raise_client_error(self, r, /):
        if r.status_code == 404:
            raise errors.ObjectNotFound(r.json().get('message', ""))
        elif r.status_code == 403:
            raise errors.RestrictedResource(r.json().get('message', ""))
        elif r.status_code == 401:
            raise errors.Unauthorized(r.json().get('message', ""))
        elif r.status_code == 409:
            raise errors.ConflictError(r.json().get('message', ""))
        elif r.status_code == 429:
            raise errors.RateLimited(r.json().get('message', ""))
        elif r.status_code == 400:
            error_details = r.json()
            error_code = error_details.get('code', 'unknown')
            error_message = error_details.get('message', "")
            if error_code == 'validation_error':
                raise errors.ValidationError(error_message)
            elif error_code == 'invalid_grant':
                raise errors.InvalidGrant(error_message)
            else:
                raise errors.BadRequest(error_message, error_code)

    def _raise_server_error(self, r, /):
        try:
            error_details = r.json()
        except Exception:
            error_code = 'unknown'
            error_message = r.text
        else:
            error_code = error_details.get('code', 'unknown')
            error_message = error_details.get('message', "")
        if r.status_code == 500:
            raise errors.InternalServerError(error_message, error_code)
        else:
            raise errors.ServiceUnavailable(
                status=r.status_code,
                code=error_code,
                message=error_message,
            )

    def create_oauth_token(self, *, code, grant_type, redirect_uri):
        raise NotImplementedError("oauth/token endpoint is not yet implemented")

    def append_block_children(self, block_id, *, children, after=None):
        payload = self._ignore_empty_fields({
            'children': children,
            'after': after,
        })
        return self._request('PATCH', f'blocks/{block_id}/children', json=payload)

    def retrieve_block(self, block_id):
        return self._request('GET', f'blocks/{block_id}')

    def _retrieve_block_children(self, block_id, *, start_cursor, page_size):
        query_dict = self._ignore_empty_fields({
            'start_cursor': start_cursor,
            'page_size': page_size,
        })
        query_string = '&'.join(f'{k}={v}' for k, v in query_dict.items())
        return self._request('GET', f'blocks/{block_id}/children?{query_string}')

    def retrieve_block_children(self, block_id, *, limit=None):
        return self._iter_results_pages(
            self._retrieve_block_children,
            block_id,
            limit=limit,
        )

    def update_block(self, block_id, **payload):
        return self._request('POST', f'blocks/{block_id}', json=payload)

    def delete_block(self, block_id):
        return self._request('DELETE', f'blocks/{block_id}')

    def create_page(self, *, parent, properties, children=None, icon=None, cover=None):
        assert (
            isinstance(parent, dict)
            and len(parent) == 1
            and ('page_id' in parent or 'database_id' in parent)
        )
        payload = self._ignore_empty_fields({
            'parent': parent,
            'properties': properties,
            'children': children,
            'icon': icon,
            'cover': cover,
        })
        return self._request('POST', 'pages', json=payload)

    def retrieve_page(self, page_id):
        return self._request('GET', f'pages/{page_id}')

    def _retrieve_page_property(self, page_id, property_id, *, start_cursor, page_size):
        query_dict = self._ignore_empty_fields({
            'start_cursor': start_cursor,
            'page_size': page_size,
        })
        query_string = '&'.join(f'{k}={v}' for k, v in query_dict.items())
        return self._request('GET', f'pages/{page_id}/properties/{property_id}?{query_string}')

    def retrieve_page_property(self, page_id, property_id, *, limit=None):
        return self._iter_results_pages(
            self._retrieve_page_property,
            page_id,
            property_id,
            limit=limit,
        )

    def update_page_properties(self, page_id, *,
                               properties=None, archived=None, icon=None, cover=None):
        payload = self._ignore_empty_fields({
            'properties': properties,
            'archived': archived,
            'icon': icon,
            'cover': cover,
        })
        return self._request('PATCH', f'pages/{page_id}', json=payload)

    def archive_page(self, page_id):
        return self.update_page_properties(page_id, archived=True)

    def create_database(self, *, parent, properties, title=None, icon=None, cover=None):
        payload = self._ignore_empty_fields({
            'parent': parent,
            'properties': properties,
            'title': title,
            'icon': icon,
            'cover': cover,
        })
        return self._request('POST', 'databases', json=payload)

    def _query_database(self, database_id, *, filter_properties=None, **payload):
        if filter_properties is not None:
            raise NotImplementedError("filter_properties is not yet implemented")
        payload = self._ignore_empty_fields(payload)
        return self._request('POST', f'databases/{database_id}/query', json=payload)

    def query_database(self, database_id, *, filter_properties=None, filter=None, sorts=None,
                       limit=None):
        return self._iter_results_pages(
            self._query_database,
            database_id,
            filter_properties=filter_properties,
            filter=filter,
            sorts=sorts,
            limit=limit,
        )

    def retrieve_database(self, database_id):
        return self._request('GET', f'databases/{database_id}')

    def update_database(self, database_id, *, properties=None, title=None, description=None):
        payload = self._ignore_empty_fields({
            'properties': properties,
            'title': title,
            'description': description,
        })
        return self._request('PATCH', f'databases/{database_id}', json=payload)

    def _list_users(self, *, start_cursor, page_size):
        query_dict = self._ignore_empty_fields({
            'start_cursor': start_cursor,
            'page_size': page_size,
        })
        query_string = '&'.join(f'{k}={v}' for k, v in query_dict.items())
        return self._request('GET', f'users?{query_string}')

    def list_users(self, *, limit=None):
        return self._iter_results_pages(self._list_users, limit=limit)

    def retrieve_user(self, user_id):
        return self._request('GET', f'users/{user_id}')

    def retrieve_me(self):
        return self.retrieve_user('me')

    def create_comment(self, *, parent=None, discussion_id=None, rich_text):
        assert (parent is None) != (discussion_id is None)
        payload = self._ignore_empty_fields({
            'parent': parent,
            'discussion_id': discussion_id,
            'rich_text': rich_text,
        })
        return self._request('POST', 'comments', json=payload)

    def _retrieve_comments(self, block_id, *, start_cursor, page_size):
        query_dict = self._ignore_empty_fields({
            'block_id': block_id,
            'start_cursor': start_cursor,
            'page_size': page_size,
        })
        query_string = '&'.join(f'{k}={v}' for k, v in query_dict.items())
        return self._request('GET', f'comments?{query_string}')

    def retrieve_comments(self, block_id, *, limit=None):
        return self._iter_results_pages(
            self._retrieve_comments,
            block_id,
            limit=limit,
        )

    def _search(self, **payload):
        return self._request('POST', 'search', json=self._ignore_empty_fields(payload))

    def search(self, query, *, sort=None, filter=None, limit=None):
        return self._iter_results_pages(
            self._search,
            query=query,
            sort=sort,
            filter=filter,
            limit=limit,
        )

    @staticmethod
    def _iter_results_pages(function, *args, limit=None, **kwargs):
        assert limit is None or limit > 0
        kwargs['start_cursor'] = None
        kwargs['page_size'] = max(limit or 0, 100)
        has_more = True
        total_count = 0
        while has_more:
            response = function(*args, **kwargs)
            if response['object'] != 'list':
                return response
            for result in response['results']:
                yield result
                total_count += 1
                if limit and total_count >= limit:
                    return
            has_more = response['has_more']
            kwargs['start_cursor'] = response['next_cursor']

    @staticmethod
    def _ignore_empty_fields(payload):
        return {k: v for k, v in payload.items() if v is not None}
