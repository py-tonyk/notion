import datetime
import re
import unicodedata

__all__ = [
    'snake_to_pascal',
    'snake_to_camel',
    'camel_to_snake',
    'unaccent',
    'identifierize',
    'parse_date_string',
]


def snake_to_pascal(snake, /):
    return snake.title().replace('_', '')


def snake_to_camel(snake, /):
    pascal = snake_to_pascal(snake)
    return pascal[:1].lower() + pascal[1:]


def camel_to_snake(camel, /):
    return re.sub(r'([^A-Z])([A-Z])', r'\1_\2', camel).lower()


def unaccent(text, /):
    return unicodedata.normalize("NFKD", str(text)).encode('ascii', 'ignore').decode('ascii')


def identifierize(name, /):
    name = unaccent(name).lower()
    name = re.sub(r'[^a-z0-9]', '_', name).strip('_')
    name = re.sub(r'_+', '_', name)
    if re.search(r'^\d', name):
        name = f'_{name}'
    return name


def parse_date_string(value, /):
    if not value:
        value = None
    elif isinstance(value, datetime.date):
        value = value
    elif len(value) == 10:
        value = datetime.date.fromisoformat(value)
    else:
        value = re.sub(r'Z$', '+00:00', value)
        value = datetime.datetime.fromisoformat(value)
    return value
