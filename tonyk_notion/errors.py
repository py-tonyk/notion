__all__ = [
    'NotionError',
    'ApiError',
    'ObjectNotFound',
    'RestrictedResource',
    'Unauthorized',
    'ConflictError',
    'BadRequest',
    'ValidationError',
    'InvalidGrant',
    'TemporaryError',
    'RateLimited',
    'NotionConnectionError',
    'ServiceUnavailable',
    'InternalServerError',
    'UnexpectedResponse',
    'NotionWarning',
    'NotionDeprecationWarning',
    'ReloadRequired',
    'DatabaseReloadRequired',
    'PageReloadRequired',
]


class NotionError(Exception):
    pass


class ApiError(NotionError):

    def __init__(self, message="", code=None, status=None):
        self.status = status or -1
        self.code = code or 'unknown'
        self.message = message


class ObjectNotFound(ApiError):

    def __init__(self, message):
        super().__init__(message, code='object_not_found', status=404)


class RestrictedResource(ApiError):

    def __init__(self, message):
        super().__init__(message, code='restricted_resource', status=403)


class Unauthorized(ApiError):

    def __init__(self, message):
        super().__init__(message, code='unauthorized', status=401)


class ConflictError(ApiError):

    def __init__(self, message):
        super().__init__(message, code='conflict_error', status=409)


class BadRequest(ApiError):

    def __init__(self, message, code):
        super().__init__(message, code=code, status=400)


class ValidationError(BadRequest):

    def __init__(self, message):
        super().__init__(message, code='validation_error')


class InvalidGrant(BadRequest):

    def __init__(self, message):
        super().__init__(message, code='invalid_grant')


class TemporaryError(ApiError):
    pass


class RateLimited(TemporaryError):

    def __init__(self, message):
        super().__init__(message, code='rate_limited', status=429)


class NotionConnectionError(ConnectionError, TemporaryError):
    pass


class ServiceUnavailable(TemporaryError):
    pass


class InternalServerError(ApiError):

    def __init__(self, message, code):
        super().__init__(message, code=code, status=500)


class UnexpectedResponse(ApiError):

    def __init__(self, status, content):
        super().__init__("Unexpected response status code: {status}", status=status)
        self.content = content


class NotionWarning(NotionError, Warning):
    pass


class NotionDeprecationWarning(DeprecationWarning, NotionWarning):
    pass


class ReloadRequired(NotionWarning):
    pass


class DatabaseReloadRequired(ReloadRequired):

    def __init__(self, *args, **kwargs):
        self.database = kwargs.pop('database', None)
        self.changes = kwargs.pop('changes', None)
        super().__init__(*args, **kwargs)


class PageReloadRequired(ReloadRequired):
    pass


class UnsupportedFeature(NotionWarning):
    pass
