import datetime
import re
import warnings

from .errors import UnsupportedFeature
from .utils import parse_date_string, camel_to_snake

__all__ = [
    'FileOrEmoji',
    'File',
    'InternalFile',
    'ExternalFile',
    'Emoji',
    'RichText',
    'RichTextElement',
    'RichTextText',
    'RichTextMention',
    'RichTextEquation',
    'Date',
    'SelectOption',
    'UniqueId',
]


class NotionObject:

    def __init__(self, raw, /):
        self._raw = raw

    def __repr__(self, /):
        class_name = self.__class__.__name__
        return f"<{class_name}: {self}>"

    def __str__(self, /):
        return str(self._raw)

    def to_json(self):
        return self._raw


class NotionJsonObject(NotionObject):

    @property
    def type(self, /):
        return self._raw['type']

    def __contains__(self, key, /):
        return key in self._raw

    def __eq__(self, value, /):
        if isinstance(value, NotionJsonObject):
            value = value._raw
        return self._raw == value

    def __iter__(self, /):
        return iter(self._raw)

    def __getitem__(self, key, /):
        try:
            return self._raw[key]
        except KeyError as e:
            raise KeyError(repr(key)) from e

    def __len__(self, /):
        return len(self._raw)

    def get(self, key, default=None, /):
        return self._raw.get(key, default)

    def items(self, /):
        return self._raw.items()

    def values(self, /):
        return self._raw.values()

    def keys(self, /):
        return self._raw.keys()


class FileOrEmoji(NotionJsonObject):

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (list, str, dict, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        if isinstance(raw, list):
            return [cls(f) for f in raw if f is not None]
        if cls is FileOrEmoji:
            if isinstance(raw, str):
                if raw.startswith(('https://', 'http://')):
                    cls = ExternalFile
                else:
                    cls = Emoji
            elif 'type' not in raw:
                warnings.warn(
                    f"Missing key 'type' in file definition: {raw!r}",
                    UnsupportedFeature,
                )
                return raw
            elif raw['type'] == 'emoji':
                cls = Emoji
            elif raw['type'] == 'file':
                cls = InternalFile
            elif raw['type'] == 'external':
                cls = ExternalFile
            else:
                warnings.warn(
                    f"Unsupported file type: {raw['type']!r}",
                    UnsupportedFeature,
                )
                return raw
        return super().__new__(cls)


class File(FileOrEmoji):

    @property
    def url(self, /):
        return self._raw[self.type]['url']

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (list, str, dict, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        if isinstance(raw, list):
            return [cls(f) for f in raw if f is not None]
        if cls is File:
            if isinstance(raw, str):
                cls = ExternalFile
            elif 'type' not in raw:
                warnings.warn(
                    f"Missing key 'type' in file definition: {raw!r}",
                    UnsupportedFeature,
                )
                return raw
            elif raw['type'] == 'file':
                cls = InternalFile
            elif raw['type'] == 'external':
                cls = ExternalFile
            else:
                warnings.warn(
                    f"Unsupported file type: {raw['type']!r}",
                    UnsupportedFeature,
                )
                return raw
        return super().__new__(cls, raw)

    def __str__(self, /):
        return self.url


class InternalFile(File):

    @property
    def expiry_time(self, /):
        return parse_date_string(self._raw[self.type].get('expiry_time'))


class ExternalFile(File):

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {'type': 'external', 'external': {'url': raw}}
        super().__init__(raw)


class Emoji(FileOrEmoji):

    @property
    def emoji(self, /):
        return self._raw['emoji']

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {'type': 'emoji', 'emoji': raw}
        super().__init__(raw)

    def __str__(self, /):
        return self.emoji


class RichText(NotionObject):

    @property
    def elements(self):
        elements = []
        if isinstance(self._raw, (str, dict)):
            raw_elements = [self._raw]
        else:
            raw_elements = self._raw
        for raw_element in raw_elements:
            if raw_element is not None:
                elements.append(RichTextElement(raw_element))
        return elements

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (list, dict, str, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        return super().__new__(cls)

    def __repr__(self):
        exerpt = str(self)
        if len(exerpt) > 80:
            exerpt = exerpt[:75] + "[...]"
        return f"<RichText: {exerpt}>"

    def __str__(self):
        return ''.join(str(element) for element in self.elements)

    def __iter__(self):
        return iter(self.elements)

    def __bool__(self):
        return bool(self.elements)

    def __len__(self):
        return len(self.elements)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if isinstance(other, (str, RichText)):
            return str(self) == str(other)
        return False

    def __contains__(self, text):
        return text in str(self)

    def __getattr__(self, name):
        return getattr(str(self), name)

    def to_json(self):
        return [element.to_json() for element in self.elements]


class RichTextElement(NotionJsonObject):

    @property
    def type(self):
        if 'type' in self._raw:
            return self._raw['type']
        else:
            return camel_to_snake(re.sub(r'^RichText', '', self.__class__.__name__))

    @property
    def plain_text(self):
        return self._raw.get('plain_text', "")

    @property
    def href(self):
        return self._raw.get('href')

    @property
    def annotations(self):
        return self._raw.get('annotations', {})

    @property
    def color(self):
        return self.annotations.get('color', 'default')

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (str, dict, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        if cls is RichTextElement:
            if isinstance(raw, str):
                cls = RichTextText
            elif 'type' not in raw:
                warnings.warn(
                    f"Missing key 'type' in rich text element definition: {raw!r}",
                    UnsupportedFeature,
                )
                return raw
            else:
                try:
                    cls = {
                        'text': RichTextText,
                        'mention': RichTextMention,
                        'equation': RichTextEquation,
                    }[raw['type']]
                except KeyError:
                    warnings.warn(
                        f"Unsupported rich text element type: {raw['type']!r}",
                        UnsupportedFeature,
                    )
                    return raw
        return super().__new__(cls)

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {'plain_text': raw}
        super().__init__(raw)

    def __str__(self):
        return self.plain_text

    def has_style(self, style, /):
        if style == 'color':
            return self.color != 'default'
        else:
            return bool(self.annotations.get(style))

    def to_json(self):
        return {
            'type': self.type,
            self.type: getattr(self, self.type),
            'annotations': self.annotations,
            'plain_text': self.plain_text,
            'href': self.href,
        }


class RichTextText(RichTextElement):

    @property
    def text(self):
        if 'text' in self._raw:
            return self._raw['text']
        else:
            return {'content': self.plain_text, 'link': self.href}

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {
                'type': 'text',
                'text': {
                    'content': raw,
                    'link': None,
                },
                'plain_text': raw,
                'href': None,
            }
        super().__init__(raw)


class RichTextMention(RichTextElement):

    @property
    def mention(self):
        return self._raw.get('mention', {})


class RichTextEquation(RichTextElement):

    @property
    def equation(self):
        return self._raw.get('equation', {})

    @property
    def expression(self):
        return self.equation.get('expression', "")

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {
                'type': 'equation',
                'equation': {
                    'expression': raw,
                },
                'plain_text': raw,
                'href': None,
            }
        super().__init__(raw)


class Date:

    @property
    def start(self):
        return parse_date_string(self._raw.get('start'))

    @property
    def end(self):
        return parse_date_string(self._raw.get('end'))

    @property
    def time_zone(self):
        # NOTE time_zone is not used as it's not documented in Notion API docs
        return self._raw.get('time_zone')

    @property
    def is_range(self):
        return self.end is not None

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (str, tuple, dict, datetime.date, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        return super().__new__(cls)

    def __init__(self, raw):
        if isinstance(raw, str):
            raw = parse_date_string(raw)
        if isinstance(raw, datetime.date):
            raw = {'start': raw}
        elif isinstance(raw, tuple):
            raw = dict(zip(('start', 'end', 'time_zone'), raw))
        self._raw = raw

    def __repr__(self):
        if self.end:
            return f"<DateRange {self.start} - {self.end}>"
        elif self.start:
            return f"<Date {self.start}>"
        else:
            return "<empty date>"

    def __str__(self):
        return " - ".join(str(d) for d in (self.start, self.end) if d is not None)

    def __eq__(self, other):
        if isinstance(other, Date):
            return self.start == other.start and self.end == other.end
        return self.start == other

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        if isinstance(other, Date):
            other = other.start
        return self.start > other

    def __ge__(self, other):
        if isinstance(other, Date):
            other = other.start
        return self.start >= other

    def __lt__(self, other):
        if isinstance(other, Date):
            other = other.start
        return self.start < other

    def __le__(self, other):
        if isinstance(other, Date):
            other = other.start
        return self.start <= other

    def __add__(self, other):
        new_start = self.start + other
        new_end = self.end + other if self.end else None
        return Date((new_start, new_end, self.time_zone))

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, datetime.date):
            if type(self.start) != type(other) and isinstance(self.start, datetime.datetime):
                return self.start.date() - other
        try:
            neg_other = -other
        except TypeError:
            return self.start - other
        else:
            return self.__add__(neg_other)

    def __rsub__(self, other):
        if isinstance(other, datetime.date):
            if type(self.start) != type(other) and isinstance(self.start, datetime.datetime):
                return other - self.start.date()
        return other - self.start

    def __bool__(self):
        return bool(self.start)

    def __format__(self, format_spec):
        if self.end:
            # Check if there are multiple occurrences of the same format codes
            format_codes = re.findall(r'%[a-zA-Z]', format_spec)
            first_duplicate = next(
                (c for i, c in enumerate(format_codes) if c in set(format_codes[:i])),
                None,
            )
            if first_duplicate:
                # If there are duplicate format codes, consider we want to format the range
                split_at = format_spec.index(
                    first_duplicate,
                    format_spec.index(first_duplicate) + 1,
                )
                start_format = format_spec[:split_at]
                end_format = format_spec[split_at:]
                return format(self.start, start_format) + format(self.end, end_format)
        return format(self.start, format_spec)

    def __hash__(self):
        if self.is_range:
            return hash((self.start, self.end))
        else:
            return hash(self.start)

    def __getitem__(self, key):
        if isinstance(key, str):
            if key not in ['start', 'end']:
                raise KeyError(f"{key!r}")
            return getattr(self, key)
        else:
            return (self.start, self.end)[key]

    def __getattr__(self, name):
        return getattr(self.start, name)

    def __reduce__(self):
        return (self.__class__, (self.start, self.end, self.time_zone))

    def to_json(self):
        if not self.start:
            return None
        json_value = {
            'start': self.start.isoformat(),
            'end': self.end.isoformat() if self.end else None,
            'time_zone': self.time_zone
        }
        return {k: v for k, v in json_value.items() if v is not None}


class SelectOption(NotionObject):

    @property
    def id(self):
        return self._raw.get('id')

    @property
    def name(self):
        return self._raw.get('name')

    @property
    def color(self):
        return self._raw.get('color')

    def __repr__(self):
        return f"<SelectOption: {self.name}>"

    def __str__(self):
        return self.name

    def __iter__(self):
        return iter(self.name)

    def __len__(self):
        return len(self.name)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, SelectOption):
            return self.id == other.id
        else:
            return self.name == other

    def __contains__(self, text):
        return text in self.name

    def __getattr__(self, name):
        try:
            return getattr(self.name, name)
        except AttributeError:
            raise AttributeError(f"'SelectOption' object has no attribute {name!r}") from None

    def to_json(self):
        return {'id': self.id, 'name': self.name, 'color': self.color}


class UniqueId(NotionObject):

    @property
    def prefix(self):
        return self._raw.get('prefix')

    @property
    def number(self):
        return self._raw['number']

    def __init__(self, raw, /):
        if isinstance(raw, int):
            raw = {'prefix': None, 'number': raw}
        elif isinstance(raw, str):
            prefix, number = raw.split('-')
            raw = {'prefix': prefix, 'number': int(number)}
        super().__init__(raw)

    def __str__(self):
        if self.prefix:
            return f"{self.prefix}-{self.number}"
        else:
            return str(self.number)

    def __eq__(self, other, /):
        if isinstance(other, int):
            return self.number == other
        if isinstance(other, (str, UniqueId)):
            return str(self) == str(other)
        return False

    def __lt__(self, other, /):
        if isinstance(other, UniqueId):
            other = other.number
        return self.number < other

    def __gt__(self, other, /):
        if isinstance(other, UniqueId):
            other = other.number
        return self.number > other

    def __le__(self, other, /):
        if isinstance(other, UniqueId):
            other = other.number
        return self.number <= other

    def __ge__(self, other, /):
        if isinstance(other, UniqueId):
            other = other.number
        return self.number >= other

    def __hash__(self):
        return hash(str(self))
