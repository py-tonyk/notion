from .datatypes import File, FileOrEmoji, RichText
from .properties import Property
from .users import User
from .utils import parse_date_string

__all__ = [
    'Page',
    'PageLoader',
]


class Page:

    @property
    def title(self):
        return self._state['properties']['title']

    @title.setter
    def title(self, value):
        self._state['properties']['title'] = RichText(value)

    @property
    def cover(self):
        return self._state['cover']

    @cover.setter
    def cover(self, value):
        self._state['cover'] = File(value)

    @property
    def icon(self):
        return self._state['icon']

    @icon.setter
    def icon(self, value):
        self._state['icon'] = FileOrEmoji(value)

    @property
    def archived(self):
        return self._state['archived']

    @archived.setter
    def archived(self, value):
        self._state['archived'] = bool(value)

    def __init__(self, _attrs=None, /, **kwargs):
        assert _attrs is None or isinstance(_attrs, (str, dict))
        if isinstance(_attrs, str):
            _attrs = {'id': _attrs}
        self._set_state({**(_attrs or {}), **kwargs})

    def __getitem__(self, key, /):
        if key in self._property_names_map:
            prop_id = self._property_names_map[key]
        else:
            prop_id = key
        try:
            return self._state['properties'][prop_id]
        except KeyError as e:
            raise KeyError(f"{self!r} has no property {key!r}") from e

    def __getattribute__(self, name, /):
        readonly_attributes = [
            'id',
            'created_time',
            'last_edited_time',
            'created_by',
            'last_edited_by',
            'url',
            'public_url',
            'properties',
        ]
        priority_attributes = ['title', 'cover', 'icon', 'archived']
        if name in readonly_attributes:
            return self._state[name]
        if name in priority_attributes:
            return getattr(Page, name).fget(self)
        else:
            return super().__getattribute__(name)

    def __setattr__(self, name, value, /):
        readonly_attributes = [
            'id',
            'created_time',
            'last_edited_time',
            'created_by',
            'last_edited_by',
            'url',
            'public_url',
            'properties',
        ]
        priority_attributes = ['title', 'cover', 'icon', 'archived']
        if name in readonly_attributes:
            raise TypeError(f"Page.{name} is readonly")
        if name in priority_attributes:
            return getattr(Page, name).fset(self, value)
        else:
            return super().__setattr__(name, value)

    def _set_state(self, state):
        self._state = {
            'object': state.get('object', 'page'),
            'id': state.get('id'),
            'created_time': parse_date_string(state.get('created_time')),
            'last_edited_time': parse_date_string(state.get('last_edited_time')),
            'created_by': User(state.get('created_by')),
            'last_edited_by': User(state.get('last_edited_by')),
            'cover': File(state.get('cover')),
            'icon': FileOrEmoji(state.get('icon')),
            'parent': state.get('parent'),
            'archived': state.get('archived'),
            'url': state.get('url'),
            'public_url': state.get('public_url'),
            'properties': {},
        }
        for prop_name, prop in state.get('properties', {}).items():
            self._set_property(prop_name, prop)

    def _set_property(self, name, value, /):
        prop = Property(value)
        self._state['properties'][prop.id] = prop.to_python(value[prop.type])
        if not hasattr(self, '_property_names_map'):
            self._property_names_map = {}
        if name not in self._property_names_map:
            self._property_names_map[name] = prop.id


class PageLoader:

    def __init__(self, client, /):
        self._client = client

    def __getitem__(self, page_id):
        page = Page(self._client.retrieve_page(page_id))
        page._client = self._client
        return page

    def search(self, query, /, *, limit=None):
        results = self._client.search(
            query=query,
            filter={'property': 'object', 'value': 'page'},
            limit=limit,
        )
        for raw_page in results:
            page = Page(raw_page)
            page._client = self._client
            yield page

    def list(self, *, limit=None):
        return self.search("", limit=limit)
