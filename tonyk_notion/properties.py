import re

from .datatypes import File, Date, RichText, SelectOption, UniqueId
from .users import User, UserList
from .utils import camel_to_snake, parse_date_string

__all__ = [
    'Property',
    'CheckboxProperty',
    'CreatedByProperty',
    'CreatedTimeProperty',
    'DateProperty',
    'EmailProperty',
    'FilesProperty',
    'FormulaProperty',
    'LastEditedByProperty',
    'LastEditedTimeProperty',
    'MultiSelectProperty',
    'NumberProperty',
    'PeopleProperty',
    'PhoneNumberProperty',
    'RelationProperty',
    'RichTextProperty',
    'RollupProperty',
    'SelectProperty',
    'StatusProperty',
    'UniqueIdProperty',
    'ButtonProperty',
    'VerificationProperty',
]


class PropertyMetaclass(type):

    _registry = {}

    def __new__(cls, name, bases, attrs):
        attrs.setdefault('_abstract', False)
        new_cls = super().__new__(cls, name, bases, attrs)
        if not new_cls._abstract:
            prop_type = re.sub('_property$', '', camel_to_snake(name))
            cls._registry[prop_type] = new_cls
        return new_cls


class Property(metaclass=PropertyMetaclass):

    _abstract = True

    @property
    def id(self):
        return self._raw.get('id')

    @property
    def name(self):
        return self._raw.get('name')

    @property
    def type(self):
        return self._raw.get('type', camel_to_snake(self.__class__.__name__))

    def __new__(cls, raw, /):
        if cls._abstract:
            cls = PropertyMetaclass._registry.get(raw['type'], cls)
        return super().__new__(cls)

    def __init__(self, raw, /):
        self._raw = raw

    def __repr__(self):
        return f"<{self.type} {self.id} \"{self}\">"

    def __str__(self):
        return self.name

    def to_python(self, value, /):
        """Convert a value from JSON to Python format."""
        return value

    def to_json(self, value, /):
        """Convert a value from Python to JSON format."""
        return value


class CheckboxProperty(Property):

    def to_json(self, value, /):
        return bool(value)


class DateProperty(Property):

    def to_python(self, value, /):
        return Date(value)

    def to_json(self, value, /):
        if value is None:
            return None
        return Date(value).to_json()


class SingleDateProperty(Property):

    _abstract = True

    def to_python(self, value, /):
        return parse_date_string(value)


class LastEditedTimeProperty(SingleDateProperty):
    pass


class CreatedTimeProperty(SingleDateProperty):
    pass


class FilesProperty(Property):

    def to_python(self, value, /):
        return File(value)

    def to_json(self, value, /):
        if isinstance(value, list):
            value = [File(f).to_json() for f in value]
        elif isinstance(value, File):
            value = [value.to_json()]
        return value


class FormulaProperty(Property):

    @property
    def expression(self):
        return self._raw.get(self.type, {}).get('expression', "")

    def to_python(self, value, /):
        python_value = value[value['type']]
        if value['type'] == 'date':
            python_value = Date(python_value)
        return python_value


class SelectProperty(Property):

    @property
    def options(self):
        # TODO Writed a wrapper for options
        return self._raw.get(self.type, {}).get('options', [])

    def to_python(self, value, /):
        if isinstance(value, dict):
            value = SelectOption(value)
        return value

    def to_json(self, value, /):
        if value is None or isinstance(value, dict):
            return value
        if isinstance(value, SelectOption):
            return value.to_json()
        json_value = None
        for option in self.options:
            if option['id'] == value:
                json_value = option
                break
            elif option['name'] == value:
                json_value = option
        if not json_value:
            raise ValueError(f"Invalid option for {self.name}: {value!r}")
        return json_value


class MultiSelectProperty(SelectProperty):

    def to_python(self, value, /):
        if isinstance(value, list):
            value = [SelectOption(option) for option in value]
        return value

    def to_json(self, value, /):
        if value is None:
            return None
        if not isinstance(value, (list, tuple)):
            value = [value]
        return [super().to_json(v) for v in filter(None, value)]


class StatusProperty(SelectProperty):

    @property
    def groups(self):
        return self._raw.get(self.type, {}).get('groups', [])


class NumberProperty(Property):

    @property
    def format(self):
        return self._raw.get(self.type, {}).get('format', 'number')


class PeopleProperty(Property):

    def to_python(self, value, /):
        if isinstance(value, list):
            return UserList(value)
        else:
            return User(value)

    def to_json(self, value, /):
        if value is None:
            value = []
        if isinstance(value, list):
            value = UserList(value)
        if isinstance(value, UserList):
            return value.to_json()
        else:
            return [User(value).to_json()]


class SinglePeopleProperty(Property):

    _abstract = True

    def to_python(self, value, /):
        return User(value)


class CreatedByProperty(SinglePeopleProperty):
    pass


class LastEditedByProperty(SinglePeopleProperty):
    pass


class RelationProperty(Property):

    @property
    def database_id(self):
        return self._raw.get(self.type, {}).get('database_id')

    @property
    def relation_type(self):
        return self._raw.get(self.type, {}).get('type')

    @property
    def synced_property_id(self):
        synced_property = self._raw.get(self.type, {}).get('dual_property', {})
        return synced_property.get('synced_property_id')

    @property
    def synced_property_name(self):
        synced_property = self._raw.get(self.type, {}).get('dual_property', {})
        return synced_property.get('synced_property_name')

    def to_python(self, value, /):
        # TODO
        return super().to_python(value)

    def to_json(self, value, /):
        if value is None:
            value = []
        if not isinstance(value, list):
            value = [value]
        json_value = []
        for item in value:
            if isinstance(item, str):
                item = {'id': item}
            elif not isinstance(item, dict):
                item = {'id': str(item.id)}
            json_value.append(item)
        return json_value


class RichTextProperty(Property):

    def to_python(self, value, /):
        return RichText(value)

    def to_json(self, value, /):
        if not value:
            return None
        return RichText(value).to_json()


class TitleProperty(RichTextProperty):
    pass


class RollupProperty(Property):

    @property
    def function(self):
        return self._raw.get(self.type, {}).get('function')

    @property
    def relation_property_id(self):
        return self._raw.get(self.type, {}).get('relation_property_id')

    @property
    def relation_property_name(self):
        return self._raw.get(self.type, {}).get('relation_property_name')

    @property
    def rollup_property_id(self):
        return self._raw.get(self.type, {}).get('rollup_property_id')

    @property
    def rollup_property_name(self):
        return self._raw.get(self.type, {}).get('rollup_property_name')

    def to_python(self, value, /):
        # TODO
        return super().to_python(value)


class UniqueIdProperty(Property):

    @property
    def prefix(self):
        return self._raw.get(self.type, {}).get('prefix')

    def to_python(self, value, /):
        return UniqueId(value)


class StringProperty(Property):
    _abstract = True


class EmailProperty(StringProperty):
    pass


class PhoneNumberProperty(StringProperty):
    pass


class UrlProperty(StringProperty):
    pass


class ButtonProperty(Property):
    pass


class VerificationProperty(Property):
    pass
