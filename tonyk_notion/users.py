import warnings

from .errors import UnsupportedFeature

__all__ = [
    'User',
    'Person',
    'Bot',
    'UserList',
    'UserLoader',
]


class User:

    @property
    def type(self):
        return self._raw.get('type', self.__class__.__name__.lower())

    @property
    def id(self):
        return self._raw['id']

    @property
    def name(self):
        return self._raw.get('name')

    @property
    def avatar_url(self):
        return self._raw.get('avatar_url')

    @property
    def _type_data(self):
        return self._raw.get(self.type, {})

    def __new__(cls, raw, /):
        assert raw is None or isinstance(raw, (str, dict, cls))
        if raw is None or isinstance(raw, cls):
            return raw
        if cls is User and isinstance(raw, dict):
            user_type = raw.get('type')
            if user_type == 'person':
                cls = Person
            elif user_type == 'bot':
                cls = Bot
            elif user_type is not None:
                warnings.warn(
                    f"Unsupported user type: {raw['type']!r}",
                    UnsupportedFeature,
                )
        return super().__new__(cls)

    def __init__(self, raw, /):
        if isinstance(raw, str):
            raw = {'id': raw}
        self._raw = raw

    def __str__(self):
        return self.name or self.id

    def __getitem__(self, key, /):
        try:
            return self._raw[key]
        except KeyError as e:
            raise KeyError(repr(key)) from e

    def get(self, key, default=None, /):
        return self._raw.get(key, default)

    def to_json(self):
        return {'object': 'user', 'id': self.id}


class Person(User):

    @property
    def email(self):
        return self._type_data.get('email')


class Bot(User):

    @property
    def owner_type(self):
        return self._type_data.get('owner', {}).get('type')

    @property
    def owner_workspace(self):
        return self._type_data.get('owner', {}).get('workspace')

    @property
    def workspace_name(self):
        return self._type_data.get('workspace_name')


class UserList:

    def __init__(self, user_list=None, /):
        self._list = []
        for user in (user_list or []):
            if not isinstance(user, User):
                user = User(user)
            self._list.append(user)

    def __repr__(self):
        return f"<UserList: [{self}]>"

    def __str__(self):
        return ', '.join(str(u) for u in self._list)

    def __iter__(self):
        return iter(self._list)

    def __bool__(self):
        return bool(self._list)

    def __len__(self):
        return len(self._list)

    def __getattr__(self, name, /):
        first_user = self._list[0] if self._list else None
        return getattr(first_user, name)

    def to_json(self):
        return [u.to_json() for u in self._list]


class UserLoader:

    def __init__(self, client, /):
        self._client = client
        self._users = {}

    def __getitem__(self, user_id):
        try:
            user = self._users[user_id]
        except KeyError:
            user = User(self._client.retrieve_user(user_id))
            self._register(user)
        return user

    def list(self, *, limit=None):
        for raw_user in self._client.list_users(limit=limit):
            user = User(raw_user)
            self._register(user)
            yield user

    def _register(self, user, /):
        user._client = self._client
        self._users[user.id] = user
