from .datatypes import RichText
from .utils import camel_to_snake

__all__ = [
    'Block',
    'Bookmark',
    'Breadcrumb',
    'BulletedListItem',
    'Callout',
    'ChildDatabase',
    'ChildPage',
    'Code',
    'ColumnList',
    'Divider',
    'Embed',
    'Equation',
    'File',
    'Heading1',
    'Heading2',
    'Heading3',
    'Image',
    'LinkPreview',
    'NumberedListItem',
    'Paragraph',
    'Pdf',
    'Quote',
    'SyncedBlock',
    'Table',
    'TableOfContents',
    'ToDo',
    'Toggle',
    'Video',
]


class BlockMetaclass(type):

    _registry = {}

    def __new__(cls, name, bases, attrs):
        attrs.setdefault('_abstract', False)
        new_cls = super().__new__(cls, name, bases, attrs)
        if not new_cls._abstract:
            cls._registry[camel_to_snake(name)] = new_cls
        return new_cls


class Block(metaclass=BlockMetaclass):

    _abstract = True

    @classmethod
    def from_string(cls, text, /):
        paragraphs = text.split("\n\n")
        return [Paragraph.from_string(p) for p in paragraphs]

    @property
    def type(self):
        return self._raw.get('type', camel_to_snake(self.__class__.__name__))

    @property
    def parent(self):
        return self._raw.get('parent')

    @property
    def id(self):
        return self._raw.get('id')

    @property
    def created_time(self):
        return self._raw.get('created_time')

    @property
    def created_by(self):
        return self._raw.get('created_by')

    @property
    def last_edited_time(self):
        return self._raw.get('last_edited_time')

    @property
    def last_edited_by(self):
        return self._raw.get('last_edited_by')

    @property
    def archived(self):
        return self._raw.get('archived')

    @property
    def has_children(self):
        return self._raw.get('has_children', bool(getattr(self, 'children', None)))

    def __new__(cls, raw=None, /, **kwargs):
        assert bool(raw) != bool(kwargs)
        if cls._abstract:
            block_type = (raw or kwargs)['type']
            cls = BlockMetaclass._registry.get(block_type, cls)
        return super().__new__(cls)

    def __init__(self, raw, /, **kwargs):
        self._raw = raw or kwargs or {}

    def __repr__(self):
        value = str(self)
        if value:
            value = f" = {value}"
        return f"<{self.type} {self.id}{value}>"

    def __str__(self):
        return ""

    def to_json(self):
        return self._raw


class ColorMixin:

    @property
    def color(self):
        return self._raw[self.type].get('color', 'default')


class RichTextMixin(ColorMixin):

    @classmethod
    def from_string(cls, text, /):
        block_type = camel_to_snake(cls.__name__)
        return cls({block_type: {'rich_text': RichText(text).to_json()}})

    @property
    def rich_text(self):
        return RichText(self._raw[self.type].get('rich_text'))

    def __str__(self):
        return str(self.rich_text)


class ParentMixin:

    @property
    def children(self):
        return self._raw[self.type].get('children', [])


class TitleMixin:

    @property
    def title(self):
        return self._raw[self.type].get('title', "")

    def __str__(self):
        return self.title


class CaptionMixin:

    @property
    def caption(self):
        return RichText(self._raw[self.type].get('caption'))


class UrlMixin:

    @property
    def url(self):
        return self._raw[self.type].get('url', "")

    def __str__(self):
        return self.url


class RichTextBlock(RichTextMixin, Block):
    _abstract = True


class Code(CaptionMixin, RichTextBlock):

    @property
    def language(self):
        return self._raw[self.type].get('language', 'plain_text')


class Callout(RichTextBlock):

    @property
    def icon(self):
        return self._raw[self.type].get('icon', {})


class RichTextParentBlock(ParentMixin, RichTextBlock):
    _abstract = True


class Paragraph(RichTextParentBlock):
    pass


class Quote(RichTextParentBlock):
    pass


class BulletedListItem(RichTextParentBlock):
    pass


class NumberedListItem(RichTextParentBlock):
    pass


class Toggle(RichTextParentBlock):
    pass


class ToDo(RichTextParentBlock):

    @property
    def checked(self):
        return self._raw[self.type].get('checked', False)


class HeadingBlock(RichTextBlock):

    _abstract = True

    @property
    def is_toggleable(self):
        return self._raw[self.type].get('is_toggleable', False)


class Heading1(HeadingBlock):
    pass


class Heading2(HeadingBlock):
    pass


class Heading3(HeadingBlock):
    pass


class FileBlock(Block):

    _abstract = True

    @property
    def file_type(self):
        return self._raw[self.type].get('type', 'external')

    @property
    def url(self):
        return self._raw[self.type].get(self.file_type, {}).get('url', "")

    @property
    def expiry_time(self):
        return self._raw[self.type].get(self.file_type, {}).get('expiry_time')

    def __str__(self):
        return self.url


class File(CaptionMixin, FileBlock):

    @property
    def name(self):
        return self._raw[self.type].get('name', "")


class Image(FileBlock):
    pass


class Video(FileBlock):
    pass


class Pdf(CaptionMixin, FileBlock):
    pass


class TableOfContents(ColorMixin, Block):
    pass


class LinkPreview(UrlMixin, Block):
    pass


class Embed(UrlMixin, Block):
    pass


class Bookmark(CaptionMixin, UrlMixin, Block):
    pass


class Breadcrumb(Block):
    pass


class Divider(Block):
    pass


class Equation(Block):

    @property
    def expression(self):
        return self._raw[self.type].get('expression', "")

    def __str__(self):
        return self.expression


class ChildDatabase(TitleMixin, Block):
    pass


class ChildPage(TitleMixin, Block):
    pass


class SyncedBlock(ParentMixin, Block):

    @property
    def synced_from(self):
        return self._raw[self.type].get('synced_from')

    def __str__(self):
        return self.synced_from['block_id'] if self.synced_from else ""


class Table(Block):

    @property
    def table_width(self):
        return self._raw[self.type].get('table_width', 0)

    @property
    def has_column_header(self):
        return self._raw[self.type].get('has_column_header', False)

    @property
    def has_row_header(self):
        return self._raw[self.type].get('has_row_header', False)


class TableRow(Block):
    # TODO Special

    @property
    def cells(self):
        return RichText(self._raw[self.type].get('cells'))


class ColumnList(Block):
    pass


class Column(Block):
    # TODO Special
    pass
