from .datatypes import RichText
from .users import User
from .utils import parse_date_string

__all__ = [
    'Comment',
]


class Comment:

    @property
    def id(self):
        return self._raw.get('id')

    @property
    def parent(self):
        return self._raw.get('parent')

    @property
    def discussion_id(self):
        return self._raw.get('discussion_id')

    @property
    def created_time(self):
        return parse_date_string(self._raw.get('created_time'))

    @property
    def last_edited_time(self):
        return parse_date_string(self._raw.get('last_edited_time'))

    @property
    def created_by(self):
        return User(self._raw.get('created_by'))

    @property
    def rich_text(self):
        return RichText(self._raw.get('rich_text'))

    def __init__(self, raw, /):
        self._raw = raw
