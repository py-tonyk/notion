import warnings

from .blocks import Block
from .datatypes import File, FileOrEmoji, RichText
from .errors import DatabaseReloadRequired, ObjectNotFound
from .pages import Page
from .properties import Property
from .users import User
from .utils import identifierize, parse_date_string

__all__ = [
    'Database',
    'DatabaseEntry',
    'DatabaseLoader',
]


class Database(type):

    def __new__(cls, name, bases, attrs):
        attrs.setdefault('_abstract', False)
        if not attrs['_abstract']:
            attrs.setdefault('_is_loaded', False)
            if 'id' not in attrs:
                raise TypeError("You must provide the database `id` when defining a Database.")
        return super().__new__(cls, name, bases, attrs)

    def __getitem__(self, page_id, /):
        response = self._client.retrieve_page(page_id)
        if response['parent']['type'] != 'database_id':
            raise TypeError("The requested page is not a database entry")
        if response['parent']['database_id'] != self.id:
            raise TypeError("The requested page is not part of this database")
        return self(response)

    def reload(self, raw_db=None, /):
        if raw_db is None:
            raw_db = self._client.retrieve_database(self.id)
        self.cover = File(raw_db.get('cover'))
        self.icon = FileOrEmoji(raw_db.get('icon'))
        self.created_time = parse_date_string(raw_db.get('time'))
        self.last_edited_time = parse_date_string(raw_db.get('last_edited_time'))
        self.created_by = User(raw_db.get('created_by'))
        self.last_edited_by = User(raw_db.get('last_edited_by'))
        self.title = RichText(raw_db.get('title'))
        self.description = RichText(raw_db.get('description'))
        self.is_inline = raw_db.get('is_inline')
        self.parent = raw_db.get('parent')
        self.url = raw_db.get('url')
        self.public_url = raw_db.get('public_url')
        self.archived = raw_db.get('archived')
        self.properties = {
            v['id']: Property(v)
            for k, v in raw_db.get('properties', {}).items()
        }
        self._property_names_map = {
            prop.name: prop.id for prop in self.properties.values()
        }
        if not hasattr(self, '_property_slugs_map'):
            self._property_slugs_map = {
                identifierize(prop.name): prop.id
                for prop in self.properties.values()
            }
        self._is_loaded = True

    def raw_query(self, **kwargs):
        results = self._client.query_database(self.id, **kwargs)
        return map(self, results)

    def create(self, _properties=None, /, page_content=None, **kwargs):
        properties = {}
        if _properties:
            for prop_id, value in _properties.items():
                if prop_id in self._property_names_map:
                    prop_id = self._property_names_map[prop_id]
                properties[prop_id] = value
        for prop_slug, value in kwargs.items():
            prop_id = self._property_slugs_map[prop_slug]
            properties[prop_id] = value

        json_properties = {
            k: self.properties[k].to_json(v)
            for k, v in properties.items()
        }

        if page_content is None:
            page_content = []
        if isinstance(page_content, str):
            page_content = Block.from_string(page_content)

        response = self._client.create_page(
            parent={'database_id': self.id},
            properties=json_properties,
            children=[b.to_json() for b in page_content],
        )
        return self(response)

    def as_python_class(self):
        property_slugs_map = "".join(
            f"        \"{k}\": \"{v}\",\n"
            for k, v in self._property_slugs_map.items()
        )
        output = (
            f"@notion.db.register\nclass {self.__name__}(DatabaseEntry):\n"
            f"    id = \"{self.id}\"\n"
            f"    _property_slugs_map = {{\n{property_slugs_map}    }}\n"
        )
        return output


class DatabaseEntry(Page, metaclass=Database):

    _abstract = True

    def __getattr__(self, name, /):
        try:
            prop_id = self.__class__._property_slugs_map[name]
        except KeyError as e:
            raise AttributeError(f"{self!r} has no attribute {name!r}") from e
        return self._state['properties'].get(prop_id)

    def update(self, _properties=None, /, **kwargs):
        if 'page_content' in kwargs:
            cls_name = self.__class__.__name__
            raise NotImplementedError(
                f"'page_content' cannot be updated with {cls_name}.update(), "
                "please update the blocks individually instead."
            )
        update_kwargs = {
            name: kwargs.pop(name)
            for name in ['cover', 'icon', 'archived'] if name in kwargs
        }

        properties = {}
        if _properties:
            for prop_id, value in _properties.items():
                if prop_id in self.__class__._property_names_map:
                    prop_id = self.__class__._property_names_map[prop_id]
                properties[prop_id] = value
        for prop_slug, value in kwargs.items():
            prop_id = self.__class__._property_slugs_map[prop_slug]
            properties[prop_id] = value

        json_properties = {
            k: self.__class__.properties[k].to_json(v)
            for k, v in properties.items()
        }
        if json_properties:
            update_kwargs['properties'] = json_properties

        if update_kwargs:
            attrs = self._client.update_page_properties(self.id, **update_kwargs)
        else:
            attrs = self._client.retrieve_page(self.id)
        self._set_state(attrs)

    def _set_property(self, name, value, /):
        database = self.__class__
        prop = database.properties.get(value['id'])
        warning_message = None
        warning_changes = {}

        if not prop:
            warning_changes['new_properties'] = {
                value['id']: {
                    'type': value['type'],
                    'name': name,
                },
            }
            warning_message = (
                f"Property {value['id']!r} not found for database {database!r}. "
                "The database schema has changed, please reload the database."
            )
        elif prop.type != value['type'] or prop.name != name:
            changes = {}
            verbose_changes = []
            if prop.type != value['type']:
                changes['type'] = value['type']
                verbose_changes.append(f"from type {prop.type!r} to {value['type']!r}")
            if prop.name != name:
                changes['name'] = name
                verbose_changes.append(f"from name {prop.name!r} to {name!r}")
            warning_message = (
                f"Property {value['id']!r} of database {database!r} has changed "
                + " and ".join(verbose_changes)
                + ". The database schema has changed, please reload the database."
            )
            warning_changes['changed_properties'] = {prop.id: changes}

        if warning_message:
            warnings.warn(
                DatabaseReloadRequired(
                    warning_message,
                    database=database,
                    changes=warning_changes,
                )
            )
            super()._set_property(name, value)
        else:
            self._state['properties'][prop.id] = prop.to_python(value[prop.type])


class DatabaseLoader:

    def __init__(self, client, /):
        self._client = client
        self._databases = {}
        self._database_names_map = {}

    def __getitem__(self, database_id):
        return self._get_database(id=database_id)

    def __getattr__(self, name):
        try:
            return self._get_database(name=name)
        except ObjectNotFound:
            raise AttributeError(f"'DatabaseLoader' object has no attribute '{name}'") from None

    def search(self, name, /):
        results = self._client.search(
            query=name,
            filter={'property': 'object', 'value': 'database'},
        )
        for raw_db in results:
            yield self._get_database(id=raw_db['id'], default=raw_db)

    def list(self):
        return self.search("")

    def register(self, database, /):
        database._client = self._client
        db_id = self._normalize_id(database.id)
        self._databases[db_id] = database
        self._database_names_map[database.__name__] = db_id

    def _get_database(self, *, id=None, name=None, default=None):
        assert (id is None) != (name is None)
        database = None
        if name:
            try:
                database = self._get_database_by_name(name)
            except ObjectNotFound:
                if default is None:
                    raise
        else:
            try:
                database = self._databases[self._normalize_id(id)]
            except KeyError:
                default = default or self._client.retrieve_database(id)
        if database is None:
            database = self._auto_register(default)
        if not database._is_loaded:
            database.reload(default)
        return database

    def _get_database_by_name(self, name, /):
        try:
            return self._databases[self._database_names_map[name]]
        except KeyError:
            for database in self.search(name):
                if database.__name__ == name:
                    return database
        raise ObjectNotFound("No database found with name {name!r}") from None

    def _auto_register(self, raw_db, /):
        name = identifierize(RichText(raw_db['title']))
        database = type(name, (DatabaseEntry,), {'id': raw_db['id']})
        self.register(database)
        database.reload(raw_db)
        return database

    @staticmethod
    def _normalize_id(id, /):
        return id.replace('-', '').lower()
